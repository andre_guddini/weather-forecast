const weatherContainer = document.querySelector('#weather');
const cityInput = document.querySelector('#city');
const getWeatherBtn = document.querySelector('.get-weather')

getWeatherBtn.addEventListener('click',processWeatherInfo);
  async  function processWeatherInfo(){
        weatherContainer.innerHTML = `
        <div class="weather-loading">
         <img src="./loading.gif" alt="loading...">
         </div>`
 let city = cityInput.value;
 const server = `https://api.openweathermap.org/data/2.5/weather?units=metric&q=${city}&appid=c5e8d9e3b67796822233879d160e09f0`
 const response = await fetch(server, {
     method: 'GET',       
 });
 const responceResult = await response.json();

 if (response.ok) {
     getWeather(responceResult);
 } else{
     weatherContainer.innerHTML = responceResult.message;
 }
    }
   


function getWeather(data){
    const location = data.name;
    const temp = Math.round(data.main.temp);
    const feelsLike = Math.round(data.main.feels_like);
    const weatherStatus = data.weather[0].main;
    const weatherIcon = data.weather[0].icon; 

    const template = `<div class="weather-header">
    <div class="weather-main">
        <div class="weather-city">${location}</div>
        
    </div>
    <div class="weather-icon">
        <img src="http://openweathermap.org/img/w/${weatherIcon}.png" alt="${weatherIcon}"> 
    </div>
<div class="weather-status">${weatherStatus}</div>
</div>
<div class="weather-temp">${temp}</div>
<div class="weather-feels-like-temp">Feels like: ${feelsLike}</div>`;
weatherContainer.classList.remove('hidden');    
weatherContainer.innerHTML = template; 
}
